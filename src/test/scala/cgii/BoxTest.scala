package cgii

/**
 * Created by bing on 10/19/15.
 */
class BoxTest extends UnitSpec{

  "BoxCutter" should "work correctly" in {
    val s =  "123cg678CG1234cg78CGcg3456CG12cg5678"
    val result = BoxCutter.cut(s)
    result.length should be(2)
    result(0).headIndex should be(8)
    result(0).tailIndex should be(17)
    result(0).seq should be("CG1234cg78")
    result(1).headIndex should be(18)
    result(1).tailIndex should be (25)
    result(1).seq should be("CGcg3456")

  }

}

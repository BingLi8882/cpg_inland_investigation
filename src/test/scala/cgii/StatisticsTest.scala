package cgii

/**
* Created by bing on 10/19/15
*/
class StatisticsTest extends UnitSpec{

  "countBoxJump" should "work correctly" in{
    val box = "12345678CG1234567CG12345678CG12345678CG12345678CG12345678"
    val cgi = """chr1 8 27 CGI 111 222 333 444
                |chr1 17 37 CGI 33 44 5
                |chr2 28 48 CGI 11 66
                |""".stripMargin


    val results = Statistics.countBoxJumps(cgi,"chr1",box)

    results.size should be(2)

    results(0).origin should be(9)
    results(0).destination should be(10)
    results(0).time should be(1)

    results(1).origin should be(10)
    results(1).destination should be(10)
    results(1).time should be(1)

  }

  it should "works correctly with threshold factor" in {

    val box = "1CG12CG123CG1234CG123CG12CG1CG123CG12CG1234"

    val cgi = """chr1 1 28 CGI 111 222 333 444
                |""".stripMargin

    val results = Statistics.countBoxJumps(cgi,"chr1",box,enableThreshold = true,threshold = 5)

    results.size should be(4)

//    results.foreach(e=>println(s"${e.origin}->${e.destination}=${e.time}"))

    results(0).origin should be(-1)
    results(0).destination should be(-1)
    results(0).time should be(2)

    results(1).origin should be(-1)
    results(1).destination should be(4)
    results(1).time should be(1)

    results(2).origin should be(4)
    results(2).destination should be(-1)
    results(2).time should be(1)

    results(3).origin should be(4)
    results(3).destination should be(3)
    results(3).time should be(1)



  }

  "countBoxes" should "works correctly" in{


    val box = "1CG12CG123CG1234CG123CG12CG1CG123CG12CG1234"

    val cgi = """chr1 1 28 CGI 111 222 333 444
                |""".stripMargin

    val results = Statistics.countBoxes(cgi,"chr1",box)


    results.size should be(4)

    results(0)._1 should be(3)
    results(0)._2 should be(1)

    results(1)._1 should be(4)
    results(1)._2 should be(2)

    results(2)._1 should be(5)
    results(2)._2 should be(2)

    results(3)._1 should be(6)
    results(3)._2 should be(1)

    val results2 = Statistics.countBoxes(cgi,"chr1",box,enableThreshold = true, threshold = 5)

    results2.size should be(3)

    results2(0)._1 should be(-1)
    results2(0)._2 should be(3)

    results2(1)._1 should be(3)
    results2(1)._2 should be(1)

    results2(2)._1 should be(4)
    results2(2)._2 should be(2)


  }
}

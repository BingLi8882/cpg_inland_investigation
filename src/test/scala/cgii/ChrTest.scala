package cgii

/**
 * Created by bing on 10/19/15.
 */
class ChrTest extends UnitSpec{

  "ChrLoader" should "work correctly" in{
    val source = "chr1 1 13 CGI 11 111 111 111\nchr1 33 44 CGI 111 11 111\nchr2\t55 66 CGI 11 11 111\n"
    val results = ChrLoader.load(source,"chr1")

    results.size should be(2)
    results(0)._1 should be(1)
    results(0)._2 should be(13)
    results(1)._1 should be(33)
    results(1)._2 should be(44)

  }
}

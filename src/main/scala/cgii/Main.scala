package cgii

import scala.io.Source

/**
 * Created by bing on 10/19/15.
 */
object Main {
  def main(args: Array[String]): Unit = {//cgi,chr,dna,out
    val cgi = Source.fromFile(args(0)).mkString
    val dna = Source.fromFile(args(2)).mkString
    val chr = args(1)
    val out = new java.io.FileWriter(args(3))

    val enableThreshold = if(args.length==5) true else false
    val threshold = if(enableThreshold)args(4).toInt else 0

    val result = Statistics.countBoxJumps(cgi,chr,dna,enableThreshold,threshold)

    result.foreach(e=>out.write(e+"\n"))
    out.close()

  }
}

package cgii

/**
 * Created by bing on 10/19/15.
 */
object BoxCutter {

  /**
   * Cut a given DNA sequence
   * @param seq a string object represent the DNA sequence
   * @return a box list stores all box in given sequence
   */
  def cut(seq: String): Array[Box] = {
    val list = seq.split("CG")
    var index = list(0).length-1
    list.slice(1, list.size - 1).map(e => {
      val s = index+1
      index += 2 + e.length
      new Box(s,index,"CG"+e)
    })
  }

}

/**
 * This object represent the information of one box
 * @param headIndex an int number represent the index of first element
 * @param tailIndex an int number represent the index of last element
 * @param seq a string value represent the box sequence
 */
class Box(val headIndex: Int, val tailIndex: Int, val seq: String) {
  /**
   * override the toString method
   * @return a string value "headindex tailinde sequence"
   */
  override def toString = headIndex + " " + tailIndex + " " + seq
}

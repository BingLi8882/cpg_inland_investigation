package cgii

import scala.collection.mutable

/**
 * Created by bing on 10/19/15.
 */
object Statistics {

  /**
   * count box jump data
   * @param cgi a string object represents cgi information
   * @param chr a string object represents chr#
   * @param boxes a string object represents DNA sequence
   * @param enableThreshold enable Threshold factor, replace all length that greater than threshold by -1
   * @param threshold a int value represents threshold
   * @return an List of BoxJumpInfo
   */
  def countBoxJumps(cgi: String, chr: String, boxes: String, enableThreshold: Boolean = false, threshold: Int = 300): List[BoxJumpInfo] =
    (if (enableThreshold) getBoxList(ChrLoader.load(cgi, chr), BoxCutter.cut(boxes),getJumpBoxes).map(e => { // enable threshold
        (e._1 match {
          case t if t >= threshold => -1
          case t: Int => t
        }, e._2 match {
          case t if t >= threshold => -1
          case t: Int => t
        })//change large number to -1 (mark of threshold)
      }) else getBoxList(ChrLoader.load(cgi, chr), BoxCutter.cut(boxes),getJumpBoxes))// without threshold
      .groupBy(e => e) // count times
      .map(e => new BoxJumpInfo(e._1._1, e._1._2, e._2.size)).toList // convert to BoxJumpInfo List
      .sortWith(_.destination<_.destination).sortWith(_.origin < _.origin) // sort

  /**
   * count box data
   * @param cgi a String object represents cgi information
   * @param chr a String object represents chr#
   * @param boxes a String object represents DNA sequence
   * @param enableThreshold enable Threshold factor, replace all length that greater than threshold by -1
   * @param threshold a int value represents threshold
   * @return a List of (Int,Int), box_length and times
   */

  def countBoxes(cgi: String, chr: String, boxes: String, enableThreshold: Boolean = false, threshold: Int = 300):List[(Int,Int)] =
    (if(enableThreshold) getBoxList(ChrLoader.load(cgi,chr),BoxCutter.cut(boxes),getBoxes).map(e=>if(e>= threshold) -1 else e) // enable threshold
    else getBoxList(ChrLoader.load(cgi,chr),BoxCutter.cut(boxes),getBoxes))// without threshold
      .groupBy(e=>e).map(e=>(e._1,e._2.size)).toList.sortWith(_._1<_._1)



  /**
   * get the boxes list of target CGI
   * @param e target CGI
   * @param boxes the DNA sequence
   * @return a list of boxes
   */
  def getBoxes(e: (Int, Int), boxes: Array[Box]): List[Int] = {
    /**
     * find the boxes list
     * @param index current index
     * @return a list of boxes
     */
    def find(index: Int): List[Int] =
      if (index >= boxes.length) {
        // out range
        throw new Exception("Can not find Target CGI in boxes " + index)
      }
      else boxes(index) match {
        case b if b.tailIndex < e._1 => find(index + 1) // before the CGI
        case b if b.headIndex <= e._2 && b.tailIndex >= e._2 => List() // the end of CGI
        case _ => (boxes(index).seq.length) :: find(index + 1) // inside the CGI
      }
    find(0)
  }


  /**
   * get a list of target boxes
   * @param cgi the CGI list
   * @param boxes the Box List
   * @param f a function for retrieving target list
   * @tparam T the element type of return sequence
   * @return an array sequence of targets
   */
  private def getBoxList[T](cgi: Array[(Int, Int)], boxes: Array[Box], f: ((Int, Int), Array[Box]) => List[T]): mutable.ArraySeq[T] =
    cgi.flatMap(f(_, boxes))


  /**
   * get all jump length in one CGI
   * @param e the start and end index
   * @return an array of all boxes length
   */
  private def getJumpBoxes(e: (Int, Int), boxes: Array[Box]): List[(Int, Int)] = {
    /**
     * recursion function to find the box list of one given CGI
     * @param index current index in boxes array
     * @param start whether enter the CGI region
     * @param length the length of previous box
     * @return a list of (length of origin, length of destination)
     */
    def find(index: Int, start: Boolean, length: Int): List[(Int, Int)] =
      if (index >= boxes.length) {
        // out range
        throw new Exception("Can not find Target CGI in boxes " + index)
      }
      else if (!start) {
        //outside of the CGI range
        if (boxes(index).headIndex <= e._1 && boxes(index).tailIndex >= e._1) {
          //check whether is bounder
          find(index + 1, true, boxes(index).seq.length)
        } else {
          find(index + 1, false, 0)
        }
      } else {
        // inside the target CGI
        if (boxes(index).headIndex <= e._2 && boxes(index).tailIndex >= e._2) {
          //check whether is bounder
          List()
        } else {
          // add (length.length) into list
          (length, boxes(index).seq.length) :: find(index + 1, true, boxes(index).seq.length)
        }
      }

    find(0, false, 0) // start from first element
  }


}


/**
 * this object represents Box jump infomation
 * @param origin the length of origin box
 * @param destination the length of destination box
 * @param time jump times
 */
class BoxJumpInfo(val origin: Int, val destination: Int, val time: Int) {
  /**
   * override toString method
   * @return a String value "From $origin to $destination jumps $time times"
   */
  override def toString = s"From ${if (origin < 0) "Threshold" else origin} to ${if (destination < 0) "Threshold" else destination} jumps $time times"
}

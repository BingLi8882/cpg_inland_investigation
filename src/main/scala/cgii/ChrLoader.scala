package cgii

/**
 * Created by bing on 10/19/15.
 */
object ChrLoader {
  /**
   * load a branch mark file
   * @param source a String object represents branch mark file
   * @param chr a String object represents target chr#
   * @return an Array stores all CGI start and end index
   */
  def load(source: String, chr: String): Array[(Int, Int)] =
    source.split("\\n").map(_.split("\\s+")).filter(_ (0) == chr).map(e => (e(1).toInt, e(2).toInt))

}
